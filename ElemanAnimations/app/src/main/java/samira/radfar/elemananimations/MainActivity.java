package samira.radfar.elemananimations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAlpha = (Button) findViewById(R.id.btnAlpha);
        btnAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView image = (ImageView) findViewById(R.id.image);
                Animation animation = AnimationUtils.loadAnimation(MainActivity.this , R.anim.simple_anim);
                image.startAnimation(animation);
            }
        });

        Button btnIntpr = (Button) findViewById(R.id.btnIntpr);
        btnIntpr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView image = (ImageView) findViewById(R.id.image);

                Animation animation = AnimationUtils.loadAnimation(MainActivity.this , R.anim.simple_anim);
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                image.startAnimation(animation);
            }
        });

        Button btnRotate = (Button) findViewById(R.id.btnrotate);
        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView image = (ImageView) findViewById(R.id.image);

                    /*                Animation anim = AnimationUtils.loadAnimation(MainActivity.this , R.anim.simple_anim);
                anim.setInterpolator(new AccelerateDecelerateInterpolator());
                tvAnim.startAnimation(anim);*/

                // AlphaAnimation animation = new AlphaAnimation(0f,1f);

                RotateAnimation animation = new RotateAnimation(0f,360f,50f,50f);
                animation.setRepeatCount(5);
                animation.setRepeatMode(Animation.INFINITE);
                animation.setDuration(500);

                image.startAnimation(animation);
            }
        });

    }
}
